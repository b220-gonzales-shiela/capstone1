# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method


from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def full_name(self, fullname):
        pass

    @abstractclassmethod
    def add_request(self):
        pass 

    @abstractclassmethod
    def check_request(self) :
        pass   

    @abstractclassmethod
    def add_user(self) :
        pass

    @abstractclassmethod
    def login(self) :
        pass

    @abstractclassmethod
    def logout(self) :
        pass



# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added


class Employee(Person):
    def __init__(self, firstname, lastname, email, department):
        super().__init__()
        self._firstname = firstname
        self._lastname = lastname
        self._email = email
        self._department = department
    
    def get_firstname(self):
        return self._firstname

    def get_lastname(self):
        return self._lastname
    
    def get_email(self):
        return self._email

    def get_department(self) :
        return self._department

    def set_firstname(self, firstname):
        self._firstname = firstname
    
    def set_lastname(self, lastname):
        self._lastname = lastname

    def set_email(self, email):
        self._email = email

    def set_department(self, department) :
        self._department= department


    def get_details(self):
        print(f"{self._firstname} {self._lastname} with an email {self._email} belongs to the department of {self._department}" )

    def full_name(self, firstname, lastname):
        print(f"{self._firstname} {self._lastname}")

    def add_request(self):
        print(f"request has been added")

    def check_request(self):
        print(f"checking request!")

    def add_user(self):
        print(f"User {self._firstname} {self._lastname} has been added")

    def login(self):
        print(f"{self._email} has been logged in")

    def logout(self) :
        print(f"{self._email} has been logged out.")

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

employee1.get_details()
employee2.get_details()
employee3.get_details()
employee4.get_details()

# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members
#   b. Methods
#       Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added
class Team_Lead(Person):

    def __init__(self, firstname, lastname, email, department):
        super().__init__()
        self._firstname = firstname
        self._lastname = lastname
        self._email = email
        self._department = department

    def get_firstname(self):
        return self._firstname

    def get_lastname(self):
        return self._lastname
    
    def get_email(self):
        return self._email

    def get_department(self) :
        return self._department

    def set_firstname(self, firstname):
        self._firstname = firstname
    
    def set_lastname(self, lastname):
        self._lastname = lastname

    def set_email(self, email):
        self._email = email

    def set_department(self, department) :
        self._department= department

    def get_details(self):
        print(f"{self._firstname} {self._lastname} with an email {self._email} is the TEAM LEAD of {self._department} Department" )

    def full_name(self, firstname, lastname):
        print(f"{self._firstname} {self._lastname}")

    def add_request(self):
        print(f"request has been added")

    def check_request(self):
        print(f"checking request!")

    def add_user(self):
        print(f"User {self._firstname} {self._lastname} has been added")

    def login(self):
        print(f"{self._email} has been logged in")

    def logout(self) :
        print(f"{self._email} has been logged out.")



class Team_Member(Person):
    def occupation(self):
        print("Team Member")

    def hasAuth(self):
        print(False)

teamLead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales")

teamLead1.get_details()

#4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class Admin(Person):

    def __init__(self, firstname, lastname, email, department):
        super().__init__()
        self._firstname = firstname
        self._lastname = lastname
        self._email = email
        self._department = department

    def get_firstname(self):
        return self._firstname

    def get_lastname(self):
        return self._lastname
    
    def get_email(self):
        return self._email

    def get_department(self) :
        return self._department

    def set_firstname(self, firstname):
        self._firstname = firstname
    
    def set_lastname(self, lastname):
        self._lastname = lastname

    def set_email(self, email):
        self._email = email

    def set_department(self, department) :
        self._department= department

    def get_details(self):
        print(f"{self._firstname} {self._lastname} with an email {self._email} is the ADMIN of {self._department} Department" )

    def full_name(self, firstname, lastname):
        print(f"{self._firstname} {self._lastname}")

    def add_request(self):
        print(f"request has been added")

    def check_request(self):
        print(f"checking request!")

    def add_user(self):
        print(f"User {self._firstname} {self._lastname} has been added")

    def login(self):
        print(f"{self._email} has been logged in")

    def logout(self) :
        print(f"{self._email} has been logged out.")


    def is_admin(self):
        print(True)

    def user_type(self):
        print("Admin User")

user_admin = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")

user_admin.get_details()

class Customer():
    def is_admin(self):
        print(False)

    def user_type(self):
        print("Regular User")

# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled
class Request(Person):

    def __init__(self, name, requester, daterequested, status):
        super().__init__()
        self._name = name
        self._requester = requester
        self._date_requested = daterequested
        self._status = status

    def get_name(self):
        return self._name

    def get_requester(self):
        return self._requester
    
    def get_daterequested(self):
        return self._daterequested

    def get_status(self) :
        return self._status

    def set_name(self, name):
        self._name = name
    
    def set_requester(self, requester):
        self._requester = requester

    def set_daterequested(self, daterequested):
        self._date_requested = daterequested

    def set_status(self, status) :
        self._status= status

    def get_details(self):
        print(f"{self._name} {self._requester}  {self._date_requested} request status{self._status}" )

    def full_name(self, firstname, lastname):
        print(f"{self._firstname} {self._lastname}")

    def login(self):
        print(f"{self._email} has been logged in")

    def logout(self) :
        print(f"{self._email} has been logged out.")

    def add_request(self):
        return (f"request has been added")

    def check_request(self):
        print(f"checking request!")

    def add_user(self):
        print(f"User {self._firstname} {self._lastname} has been added")

request= Request("Monika", "oct202021", "laptop repair", "closed")

request.get_details()